import express, {Request,Response} from "express";
import { body,validationResult } from "express-validator";
import { RequestValidationError } from "../errors/request-validation-error";
import { validateRequest } from "../middelware/validate-request";
import { User } from "../models/user";
import { BadRequestError } from "../errors/bad-request-error";
import { Password } from "../services/password";
import  jwt  from "jsonwebtoken";
const router = express.Router();

router.post("/api/users/signin", 
[
  body('email')
  .isEmail()
  .withMessage('email must be valid'),
  body('password')
  .trim()
  .notEmpty()
  .withMessage('password must be valid')
],
validateRequest,



async (req:Request, res:Response) => {
  const{email,password}=req.body
  const existingUser =await User.findOne({email})
  if (!existingUser) {
throw new BadRequestError('invalid credts')
  }

  const passwordMatch =await Password.compare(existingUser.password,password)
  if (!passwordMatch) {
    throw new BadRequestError('invalid creds')
  }
   //generate jwt
    

const userJwt = jwt.sign({
  id:existingUser.id,
  email:existingUser.email
}, process.env.JWT_KEY!
) 

    //store it on session object

    req.session= {
      jwt:userJwt
    }

    res.status(200).send(existingUser);
}
);

export { router as signinRouter };
